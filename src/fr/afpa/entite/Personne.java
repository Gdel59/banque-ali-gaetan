package fr.afpa.entite;

public class Personne {
	
	
	protected String nom;
	protected String prenom;
	protected String dateNaissance;
	protected String email;
	protected String identifiant;
	protected String motDePasse;
	
	
	public Personne(String nom, String prenom, String dateNaissance, String email, String identifiant,
			String motDePasse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.identifiant = identifiant;
		this.motDePasse = motDePasse;
	}
	
	
	
	public Personne() {
		super();
	}



	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}



	@Override
	public String toString() {
		return "Personne [nom = " + nom + ", prenom = " + prenom + ", dateNaissance = " + dateNaissance + ", email = " + email
				+ ", identifiant = " + identifiant + "]";
	}

	

		
}
