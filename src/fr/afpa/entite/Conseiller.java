package fr.afpa.entite;

import java.util.ArrayList;

public class Conseiller extends Personne {
	
	private ArrayList<Client> listeClient;

	public Conseiller(String nom, String prenom, String dateNaissance, String email, String identifiant,
			String motDePasse) {
		super(nom, prenom, dateNaissance, email, identifiant, motDePasse);
		this.listeClient = new ArrayList<Client>() ;
	}

	
	
	

	public Conseiller() {
		super();
		this.listeClient = new ArrayList<Client>() ;
	}

	public Conseiller(String nom, String prenom, String dateNaissance, String email, String identifiant,
			String motDePasse, ArrayList<Client> listeClient) {
		super(nom, prenom, dateNaissance, email, identifiant, motDePasse);
		this.listeClient = new ArrayList<Client>() ;
	}

	public ArrayList<Client> getListeClient() {
		return listeClient;
	}

	public void setListeClient(ArrayList<Client> listeClient) {
		this.listeClient = listeClient;
	}





	@Override
	public String toString() {
		return "Conseiller [ nom=" + nom + ", prenom=" + prenom + ", identifiant="
				+ identifiant + ", listeClient=" + listeClient +" ]";
	}








	
	
	


	
}
