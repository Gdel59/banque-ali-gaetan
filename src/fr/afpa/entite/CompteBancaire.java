package fr.afpa.entite;

public class CompteBancaire {
	
	private String numeroDeCompte;
	private Boolean decouvertAutorise;
	private Float soldeCompte;
	private Boolean estActif;
	
	
	public CompteBancaire(String numeroDeCompte) {
		super();
		this.numeroDeCompte = numeroDeCompte;
	}
	
	
	public CompteBancaire() {
		super();
	}
	public CompteBancaire(String numeroDeCompte, Boolean decouvertAutorise, Float soldeCompte) {
		super();
		this.numeroDeCompte = numeroDeCompte;
		this.decouvertAutorise = decouvertAutorise;
		this.soldeCompte = soldeCompte;
		this.estActif = true;
	}
	
	@Override
	public String toString() {
		return "CompteBancaire [numeroDeCompte=" + numeroDeCompte + ", decouvertAutorise=" + decouvertAutorise
				+ ", soldeCompte=" + soldeCompte + "]";
	}
	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}
	public void setNumeroDeCompte(String numeroDeCompte) {
		this.numeroDeCompte = numeroDeCompte;
	}
	public Boolean getDecouvertAutorise() {
		return decouvertAutorise;
	}
	public void setDecouvertAutorise(Boolean decouvertAutorise) {
		this.decouvertAutorise = decouvertAutorise;
	}
	public Float getSoldeCompte() {
		return soldeCompte;
	}
	public void setSoldeCompte(Float soldeCompte) {
		this.soldeCompte = soldeCompte;
	}


	public Boolean getEstActif() {
		return estActif;
	}


	public void setEstActif(Boolean estActif) {
		this.estActif = estActif;
	}
	

}
