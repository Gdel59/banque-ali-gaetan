package fr.afpa.entite;

import java.util.ArrayList;

public class Banque {
    //attributs
	private ArrayList<Agence> listeAgence;
	private Administrateur administrateur;

	// Constructeurs
	public Banque() {
		super();
		this.listeAgence = new ArrayList<Agence>();

	}

	public Banque(Administrateur administrateur) {
		super();
		this.listeAgence = new ArrayList<Agence>();
		this.administrateur = administrateur;
	}

	@Override
	public String toString() {
		return "Banque [listeAgence=" + listeAgence + ", administrateur=" + administrateur + "]";
	}
//Getters Setters
	public ArrayList<Agence> getListeAgence() {
		return listeAgence;
	}

	public void setListeAgence(ArrayList<Agence> listeAgence) {
		this.listeAgence = listeAgence;
	}

	public Administrateur getAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}
}
