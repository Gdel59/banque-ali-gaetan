package fr.afpa.entite;

import java.util.ArrayList;

public class Agence {
	//Attributs
	private String nomAgence;
	private static int autoIncrementationDuCode = 0;
	private String codeAgence;
	private String adresseAgence;
	private ArrayList<Conseiller> listeConseiller;
	
	//Constructeurs
	public Agence(String nomAgence, String adresseAgence) {
		super();
		this.nomAgence = nomAgence;
		this.codeAgence = String.format("%03d", ++autoIncrementationDuCode);
		this.adresseAgence = adresseAgence;
		this.listeConseiller = new ArrayList<Conseiller>() ;
	}
	
	
	public Agence() {
		super();
		this.listeConseiller = new ArrayList<Conseiller>() ;

	}



//Getters Setters
	public String getNomAgence() {
		return nomAgence;
	}
	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}
	public String getCodeAgence() {
		return codeAgence;
	}
	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}
	public String getAdresseAgence() {
		return adresseAgence;
	}
	public void setAdresseAgence(String adresseAgence) {
		this.adresseAgence = adresseAgence;
	}


	public ArrayList<Conseiller> getListeConseiller() {
		return listeConseiller;
	}


	public void setListeConseiller(ArrayList<Conseiller> listeConseiller) {
		this.listeConseiller = listeConseiller;
	}


	@Override
	public String toString() {
		return "Agence [nomAgence=" + nomAgence + ", codeAgence=" + codeAgence + ", adresseAgence=" + adresseAgence
				+ ", listeConseiller=" + listeConseiller + "]";
	}
	
	
}
