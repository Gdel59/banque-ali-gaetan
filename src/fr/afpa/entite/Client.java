package fr.afpa.entite;

import java.util.ArrayList;

public class Client extends Personne {

	private String numeroClient;
	private ArrayList<CompteBancaire> listeCompteBancaire;
	private Boolean estActif;

	public Client() {
		super();
		this.listeCompteBancaire = new ArrayList<CompteBancaire>();
	}

	public Client(String nom, String prenom, String dateNaissance, String email, String identifiant, String motDePasse,
			String numeroClient) {
		super(nom, prenom, dateNaissance, email, identifiant, motDePasse);
		this.numeroClient = numeroClient;
		this.listeCompteBancaire = new ArrayList<CompteBancaire>();
		this.estActif = true;
	}

	public Client(String nom, String prenom, String dateNaissance, String email, String identifiant, String motDePasse,
			String numeroClient, ArrayList<CompteBancaire> listeCompteBancaire) {
		super(nom, prenom, dateNaissance, email, identifiant, motDePasse);
		this.numeroClient = numeroClient;
		this.listeCompteBancaire = new ArrayList<CompteBancaire>();
		
	}

	public String getNumeroClient() {
		return numeroClient;
	}

	public void setNumeroClient(String numeroClient) {
		this.numeroClient = numeroClient;
	}

	public ArrayList<CompteBancaire> getListeCompteBancaire() {
		return listeCompteBancaire;
	}

	public void setListeCompteBancaire(ArrayList<CompteBancaire> listeCompteBancaire) {
		this.listeCompteBancaire = listeCompteBancaire;
	}

	@Override
	public String toString() {
		return "Client [ identifiant=" + identifiant + ", nom=" + nom
				+ ", prenom=" + prenom + ", email=" + email +
				"numeroClient=" + numeroClient + ", listeCompteBancaire=" + listeCompteBancaire +"]";
	}



	
	
	

	public Boolean getEstActif() {
		return estActif;
	}

	public void setEstActif(Boolean estActif) {
		this.estActif = estActif;
	}

}
