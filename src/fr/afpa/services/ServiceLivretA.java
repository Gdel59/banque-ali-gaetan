package fr.afpa.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDate;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Conseiller;

public class ServiceLivretA {
	//M�thode pour calculer les frais de tenue de compte 

	public void calculerFraisLivretA(Banque banque, CompteBancaire compte) {
		CompteBancaire compteFrais = null;
		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client client : conseiller.getListeClient()) {
					for (CompteBancaire compteBancaire : client.getListeCompteBancaire()) {
						if (compte.getNumeroDeCompte().equals(compteFrais.getNumeroDeCompte())) {
							compteFrais = compte;
						}
						if (compteFrais != null) {
							compteFrais.setSoldeCompte((float) ((compteFrais.getSoldeCompte() - 25)+(compteFrais.getSoldeCompte()*1.1)));
							try {
								String date = LocalDate.now().toString();
								String naturePaiement = "D�bit";
								FileWriter fw = new FileWriter(
										"C:\\ENV\\workspace Gaetan\\banque-ali-gaetan\\Ressources\\TransactionBancaire"
												+ client.getIdentifiant() + date + ".txt");
								BufferedWriter nouvelleTransaction = new BufferedWriter(fw);
								nouvelleTransaction.write(date);
								nouvelleTransaction.newLine();
								nouvelleTransaction.write(naturePaiement);
								nouvelleTransaction.newLine();
								nouvelleTransaction.write("- 25 euros" + compteFrais.getSoldeCompte()*1.1);
								nouvelleTransaction.newLine();
								nouvelleTransaction.write(compteFrais.getNumeroDeCompte());
								nouvelleTransaction.close();

							} catch (Exception e) {
								System.out.println("Erreur" + e);
							}
						}
					}
				}
			}
		}
	}
}