package fr.afpa.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

import fr.afpa.controles.ControleOperation;
import fr.afpa.controles.ControleSaisieAgence;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PlanEpargneLogement;
import fr.afpa.ihm.AffichageMenuAdministrateur;

public class ServiceBanque {

	/**
	 * M�thode pour cr�er une Agence
	 * @param banque
	 * @return L'agence cr��e
	 */
	public static Agence creerAgence(Banque banque) {

		Scanner in = new Scanner(System.in);

		System.out.println("Veuillez saisir un nom pour l'Agence");
		String nom = ControleSaisieAgence.VerifNomAgence(in.nextLine());

		// System.out.println("Veuillez saisir un code correspondant a l'Agence");
		// String code = ControleSaisieAgence.VerifCodeAgence(in.nextLine());

		System.out.println("Veuillez saisir le numero de rue de l'Agence");
		String numero = ControleSaisieAgence.VerifnumeroRue(in.nextLine());

		System.out.println("Veuillez saisir le libelle de domiciliation de l'Agence");
		String libelle = ControleSaisieAgence.VeriflibelleRue(in.nextLine());

		System.out.println("Veuillez saisir le code postal correspondant a la ville de residence de l'Agence");
		String codePostal = ControleSaisieAgence.VerifCodePostalAgence(in.nextLine());

		System.out.println("Veuillez saisir le nom de la ville de l'Agence");
		String ville = ControleSaisieAgence.VerifVilleAgence(in.nextLine());

		String adresse = numero + " " + libelle + " " + codePostal + " " + ville + " ";

		Agence agence = new Agence(nom, adresse);

		System.out.println("Le code de cette agence est: " + agence.getCodeAgence());

		banque.getListeAgence().add(agence);
		return agence;

	}
	/**
	 * M�thode pour effectuer des virements
	 * @param compte1 Compte � d�biter
	 * @param compte2 Compte � cr�diter
	 * @param montant A transf�rer
	 * @param banque 
	 */

	public static void virement(CompteBancaire compte1, CompteBancaire compte2, Float montant, Banque banque) {
		CompteBancaire compteADebiter = null;
		CompteBancaire compteACrediter = null;

		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client client : conseiller.getListeClient()) {
					for (CompteBancaire compteBancaire : client.getListeCompteBancaire()) {
						if (compte1.getNumeroDeCompte().equals(compteBancaire.getNumeroDeCompte())) {
							compteADebiter = compte1;
						}
						if (compte2.getNumeroDeCompte().equals(compteBancaire.getNumeroDeCompte())) {
							compteACrediter = compte2;
						}
					}
				}
			}
		}
		if (compteADebiter != null && compteACrediter != null) {
			if (ControleOperation.controleSoldeValide(compteADebiter, montant)) {
				compteADebiter.setSoldeCompte(compteADebiter.getSoldeCompte() - montant);
				try {
					String date = LocalDate.now().toString();
					String naturePaiement = "D�bit";
					FileWriter fw = new FileWriter(
							"Ressources\\TransactionBancaire" + compteADebiter.getNumeroDeCompte() + date + ".txt",
							true);
					BufferedWriter nouvelleTransaction = new BufferedWriter(fw);
					nouvelleTransaction.write(date);
					nouvelleTransaction.write("~");
					nouvelleTransaction.write(naturePaiement);
					nouvelleTransaction.write("~");
					nouvelleTransaction.write((int) (compteADebiter.getSoldeCompte() - montant));
					nouvelleTransaction.write("~");
					nouvelleTransaction.write(compteADebiter.getNumeroDeCompte());
					nouvelleTransaction.newLine();
					nouvelleTransaction.close();

				} catch (Exception e) {
					System.out.println("Erreur" + e);
				}
				compteACrediter.setSoldeCompte(compteADebiter.getSoldeCompte() + montant);
				try {
					String date = LocalDate.now().toString();
					String naturePaiement = "Cr�dit";
					FileWriter fw = new FileWriter(
							"Ressources\\TransactionBancaire" + compteACrediter.getNumeroDeCompte() + date + ".txt",
							true);
					BufferedWriter nouvelleTransaction = new BufferedWriter(fw);
					nouvelleTransaction.write(date);
					nouvelleTransaction.write("~");
					nouvelleTransaction.write(naturePaiement);
					nouvelleTransaction.write("~");
					nouvelleTransaction.write((int) (compteACrediter.getSoldeCompte() + montant));
					nouvelleTransaction.write("~");
					nouvelleTransaction.write(compteACrediter.getNumeroDeCompte());
					nouvelleTransaction.newLine();
					nouvelleTransaction.close();

				} catch (Exception e) {
					System.out.println("Erreur" + e);
				}
			}
		}
	}

	/**
	 * M�thode pour cr�er un compte bancaire
	 * @param banque
	 * @return le compte cr��
	 */
	public static CompteBancaire creerCompte(Banque banque) {

		Scanner in = new Scanner(System.in);

		boolean trouveClient = false;
		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				if (agence.getListeConseiller().size() > 0) {
					trouveClient = true;
				}
			}
		}
		if (!trouveClient) {
			System.out.println("Veuillez d'abord creer un client");
			return null;
		}

		System.out.println("A quel client souhaitez vous  attribuer ce compte bancaire ?");
		String identifiantClientSaisie = in.nextLine();
		Client client1 = null;

		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client client : conseiller.getListeClient()) {

					if (identifiantClientSaisie.equals(client.getIdentifiant())) {
						client1 = client;
					}

				}

			}

			if (client1 != null) {

				if (client1.getListeCompteBancaire().size() == 0) {
					Random random = new Random();
					String codeCompteBancaire = "";
					for (int i = 0; i < 11; i++) {
						codeCompteBancaire += random.nextInt(10);
					}

					System.out.println(
							"Souhaitez vous autoriser un decouvert pour le compte : " + codeCompteBancaire + " ? ");

					boolean reponseDecouvert = false;

					System.out.println();
					System.out.println("1 - Oui");
					System.out.println("2 - Non");
					String reponse = in.nextLine();

					if (reponse.equals("1")) {
						reponseDecouvert = true;
					} else if (reponse.equals("2")) {
						reponseDecouvert = false;
					} else {
						System.out.println("Caract�re non valide");
						AffichageMenuAdministrateur.menuAdministrateur(banque);
					}

					System.out.println("Quel est le solde ? ");
					Float soldeCreation = in.nextFloat();

					CompteBancaire compte = new CompteCourant(codeCompteBancaire, reponseDecouvert, soldeCreation);
					client1.getListeCompteBancaire().add(compte);
					return compte;

				}
				if (client1.getListeCompteBancaire().size() > 0) {
					System.out.println("Quel type de compte voulez-vous ouvrir ? Livret A ou Plan Epargne Logement");
					System.out.println();
					System.out.println("1 - Livret A");
					System.out.println("2 - Plan Epargne Logement");
					String reponse = in.nextLine();

					if (reponse.equals("1")) {

						Random random = new Random();
						String codeCompteBancaire = "";
						for (int i = 0; i < 11; i++) {
							codeCompteBancaire += random.nextInt(10);
						}

						System.out.println(
								"Souhaitez vous autoriser un decouvert pour le compte : " + codeCompteBancaire + " ? ");

						boolean reponseDecouvert = false;

						System.out.println();
						System.out.println("1 - Oui");
						System.out.println("2 - Non");
						String reponse2 = in.nextLine();

						if (reponse2.equals("1")) {
							reponseDecouvert = true;
						} else if (reponse2.equals("2")) {
							reponseDecouvert = false;
						} else {
							System.out.println("Caract�re non valide");
							AffichageMenuAdministrateur.menuAdministrateur(banque);
						}

						System.out.println("Quel est le solde ? ");
						Float soldeCreation = in.nextFloat();

						CompteBancaire compte = new LivretA(codeCompteBancaire, reponseDecouvert, soldeCreation);
						client1.getListeCompteBancaire().add(compte);
						return compte;

					} else if (reponse.equals("2")) {

						Random random = new Random();
						String codeCompteBancaire = "";
						for (int i = 0; i < 11; i++) {
							codeCompteBancaire += random.nextInt(10);
						}

						System.out.println(
								"Souhaitez vous autoriser un decouvert pour le compte : " + codeCompteBancaire + " ? ");

						boolean reponseDecouvert = false;

						System.out.println();
						System.out.println("1 - Oui");
						System.out.println("2 - Non");
						String reponse2 = in.nextLine();

						if (reponse2.equals("1")) {
							reponseDecouvert = true;
						} else if (reponse2.equals("2")) {
							reponseDecouvert = false;
						}

						else {
							System.out.println("Caract�re non valide");
							AffichageMenuAdministrateur.menuAdministrateur(banque);
						}

						System.out.println("Quel est le solde ? ");
						Float soldeCreation = in.nextFloat();

						CompteBancaire compte = new PlanEpargneLogement(codeCompteBancaire, reponseDecouvert,
								soldeCreation);
						client1.getListeCompteBancaire().add(compte);
						return compte;

					} else {
						System.out.println("Caract�re non valide");
						AffichageMenuAdministrateur.menuAdministrateur(banque);
					}

				}

			}

		}

		return null;
	}

	/**
	 * M�thode pour d�sactiver un client de la banque
	 * @param banque
	 * @param client 
	 * @return true si le client est d�sactiv�
	 */
	public static boolean desactiverClient(Banque banque, Client client) {

		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client clientTmp : conseiller.getListeClient()) {
					if (clientTmp.getIdentifiant().equalsIgnoreCase(client.getIdentifiant())) {
						clientTmp.setEstActif(true);
						System.out.println("Le client" + clientTmp.getIdentifiant() + "a �t� d�sactiv�");
					}
				}
			}
		}

		return false;

	}
	/**
	 * M�thode pour d�sactiver le compte d'un client
	 * @param banque
	 * @param client 
	 * @return true si le compte est d�sactiv�
	 */

	public static boolean desactiverCompte(Banque banque, CompteBancaire compte) {

		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client client : conseiller.getListeClient()) {
					for (CompteBancaire compteBancaire : client.getListeCompteBancaire()) {
						if (compteBancaire.getNumeroDeCompte().equalsIgnoreCase(compte.getNumeroDeCompte())) {
							compteBancaire.setEstActif(true);
							System.out.println("Le compte" + compte.getNumeroDeCompte() + "a �t� d�sactiv�");
						}
					}
				}
			}

		}
		return false;

	}
}
