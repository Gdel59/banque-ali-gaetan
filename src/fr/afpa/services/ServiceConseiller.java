package fr.afpa.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Scanner;

import fr.afpa.controles.ControleSaisieClient;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;

public class ServiceConseiller {
	//M�thode pour cr�er un client

	public static Client creerClient(Banque banque) {

		Scanner in = new Scanner(System.in);
		boolean trouveConseiller = false;
		for (Agence agence : banque.getListeAgence()) {
			if (agence.getListeConseiller().size() > 0) {
				trouveConseiller = true;
			}
		}
		if (!trouveConseiller) {
			System.out.println("Veuillez d'abord creer un conseiller");
			return null;
		}

		System.out.println("A quel conseiller souhaitez vous attribuer ce client ?");
		String identifiantConseillerSaisie = in.nextLine();
		Conseiller conseiller1 = null;

		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				if (identifiantConseillerSaisie.equals(conseiller.getIdentifiant())) {
					conseiller1 = conseiller;
				}

			}

		}

		if (conseiller1 != null) {

			System.out.println("Veuillez saisir le Nom du client");
			String nom = in.nextLine();
			ControleSaisieClient.validerNom(nom);
			System.out.println("Veuillez saisir le Prenom du client");
			String prenom = in.nextLine();
			ControleSaisieClient.validerPrenom(prenom);
			System.out.println("Veuillez saisir la date de naissance du client");
			String dateNaissance = in.nextLine();
			ControleSaisieClient.verifierDateValide(dateNaissance);
			System.out.println("Veuillez saisir l'email du client");
			String email = in.nextLine();
			ControleSaisieClient.validerEmail(email);
			System.out.println("Veuillez saisir l'identifiant du client");
			String identifiant = in.nextLine();
			ControleSaisieClient.validerIdentifiant(identifiant);
			System.out.println("Veuillez saisir le mot de passe du client");
			String motDePasse = in.nextLine();
			System.out.println("Veuillez saisir le num�ro client");
			String loginClient = in.nextLine();
			try {
				FileWriter fw = new FileWriter(
						"Ressources\\LoginMdpClients.txt",true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(loginClient);
				bw.write("~");
				bw.write(motDePasse);
				bw.newLine();
				
				bw.close();
			} catch (Exception e) {
				System.out.println("Erreur" + e);
			}

			Client client = new Client(nom, prenom, dateNaissance, email, identifiant, motDePasse, loginClient);
			conseiller1.getListeClient().add(client);
			return client;
		}
		return null;
	}
}