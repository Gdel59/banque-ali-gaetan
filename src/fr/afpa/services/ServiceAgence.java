package fr.afpa.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Scanner;

import fr.afpa.controles.ControleSaisieClient;
import fr.afpa.controles.ControleSaisieConseiller;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Conseiller;

public class ServiceAgence {
	
	/**
	 * M�thode pour cr�er un conseiller
	 * @param banque
	 * @return Le conseiller cr��
	 */
	
	public static Conseiller creerConseiller(Banque banque) {
		Scanner in = new Scanner(System.in);

		if (banque.getListeAgence().size() == 0) {
			System.out.println("Veuillez d'abord creer une agence");
			return null;
		}
		System.out.println("Dans quelle agence souhaitez vous placer ce conseiller ?");
		String codeAgenceSaisie = in.nextLine();
		Agence agence1 = null;

		for (Agence agence : banque.getListeAgence()) {
			if (codeAgenceSaisie.equals(agence.getCodeAgence())) {
				agence1 = agence;
			}

		}

		if (agence1 != null) {

			System.out.println("Veuillez saisir le nom du conseiller");
			String nom = in.nextLine();
			ControleSaisieClient.validerNom(nom);
			System.out.println("Veuillez saisir le prenom du conseiller");
			String prenom = in.nextLine();
			ControleSaisieClient.validerPrenom(prenom);
			System.out.println("Veuillez saisir la date de naissance du conseiller");
			String dateNaissance = in.nextLine();
			ControleSaisieClient.verifierDateValide(dateNaissance);
			System.out.println("Veuillez saisir l'email du conseiller");
			String email = in.nextLine();
			ControleSaisieClient.validerEmail(email);
			System.out.println("Veuillez saisir l'identifiant du conseiller");
			String identifiant = in.nextLine();
			ControleSaisieConseiller.verifCreationLoginConseiller(identifiant);

			System.out.println("Veuillez saisir le mot de passe du conseiller");
			String motDePasse = in.nextLine();

			try {
				FileWriter fw = new FileWriter(
						"Ressources\\LoginMdpConseiller.txt",true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(identifiant);
				bw.write("~");
				bw.write(motDePasse);
				bw.newLine();
				bw.close();
			} catch (Exception e) {
				System.out.println("Erreur" + e);
			}
			Conseiller conseiller = new Conseiller(nom, prenom, dateNaissance, email, identifiant, motDePasse);
			agence1.getListeConseiller().add(conseiller);

			return conseiller;
		}
		return null;
	}
	
}
