package fr.afpa.services;



import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Conseiller;


public class ServiceClient {
	
//M�thode pour afficher les infos du client
	public static void afficherInfoClient(Banque banque) {
		
		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client clientTmp : conseiller.getListeClient()) {
					if (clientTmp.getIdentifiant().equals(clientTmp.getIdentifiant())) {
						
						System.out.println("Pr�nom : " + clientTmp.getPrenom());
						System.out.println("Nom : "+ clientTmp.getNom());
						System.out.println("Date de naissance : " + clientTmp.getDateNaissance());
						System.out.println("Email : " + clientTmp.getEmail());
						System.out.println("Num�ro client : " + clientTmp.getNumeroClient());
						
					}
				}
		
		
		
		
	}
}
}
	//M�thode pour afficher les infos des comptes des clients

	public static void afficherInfoCompteClient(Banque banque) {
		
		for (Agence agence : banque.getListeAgence()) {
			for (Conseiller conseiller : agence.getListeConseiller()) {
				for (Client clientTmp : conseiller.getListeClient()) {
					for (CompteBancaire compte : clientTmp.getListeCompteBancaire()){
						if (clientTmp.getIdentifiant().equals(clientTmp.getIdentifiant())) {
							
							System.out.println("Num�ro de Compte : " + compte.getNumeroDeCompte());
							System.out.println("Solde du Compte : " + compte.getSoldeCompte());
							System.out.println("D�couvert autoris� ? : " + compte.getDecouvertAutorise());
						}
					}
						
					}
				}
	}
	}
	
	
}
