package fr.afpa.ihm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import fr.afpa.controles.ControleSaisieAdministrateur;
import fr.afpa.controles.ControleSaisieClient;
import fr.afpa.controles.ControleSaisieConseiller;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Conseiller;

public class AffichageChoixProfil {
	
	/**
	 * Affichage pour choisir le profil de l'utilisateur
	 * @param banque
	 */ 
	
	public static void affichageChoix(Banque banque) {
		Scanner in = new Scanner(System.in);
		
		
		String choix = " ";
		
		System.out.println("Veuillez faire un choix de profil. Tapez 1, 2 ou 3.");
		System.out.println();
		System.out.println("1- Client");
		System.out.println("2- Conseiller");
		System.out.println("3- Administrateur");
		
		
		while(choix!="1" || choix!="2" || choix!="3") {
			choix = in.nextLine();
		switch (choix) {
		
		case "1": 
		
		
			String loginClient = null;
			String mdpClient = null;
			do {
			System.out.println("Veuillez saisir votre login Client");
			 loginClient = in.nextLine();
			}
			while(!ControleSaisieClient.verifLoginClient(loginClient));
			do {
			System.out.println("Veuillez saisir votre mot de passe Client");
			 mdpClient = in.nextLine();
			}
			while(!ControleSaisieClient.verifMdpClient(mdpClient));
			
			AffichageMenuClient.menuClient(banque);
		
			
			break;
			
		case "2": 
			
			String loginConseiller = null;
		String mdpConseiller = null;
			do {
			System.out.println("Veuillez saisir le login Conseiller");
		 loginConseiller = in.nextLine();
			}
			while(!ControleSaisieConseiller.verifLoginConseiller(loginConseiller));
			do {
			System.out.println("Veuillez saisir le mot de passe Conseiller");
			 mdpConseiller = in.nextLine();
			}
			 while(!ControleSaisieConseiller.verifMdpConseiller(mdpConseiller));
			
			AffichageMenuConseiller.AfficheMenuConseiller(banque);
				
		
		
		break;
		
		
		case "3": 
			try {
				ControleSaisieAdministrateur.validerAuthentificationAdmininistrateur(in);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		AffichageMenuAdministrateur.menuAdministrateur(banque);


		break;
		
		default: System.out.println("Veuillez saisir un caract�re valide ");
		break;
		}
		
		}
		
		
	}}
	
	
	
	
	

	
	
	
	


