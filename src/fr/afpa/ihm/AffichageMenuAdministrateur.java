package fr.afpa.ihm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.controles.ControleSaisieAdministrateur;
import fr.afpa.controles.ControleSaisieCompteBancaire;
import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.ServiceAgence;
import fr.afpa.services.ServiceBanque;
import fr.afpa.services.ServiceClient;
import fr.afpa.services.ServiceConseiller;

public class AffichageMenuAdministrateur {


	/**
	 * Affichage menu du profil Administrateur
	 * @param banque
	 */
	

	public static void menuAdministrateur(Banque banque) {
		do{ 
		System.out.println("Menu Administrateur :");
		System.out.println();
		System.out.println("1 - Consulter les informations d'un client");
		System.out.println("2 - Consulter les comptes d'un client ");
		System.out.println("3 - Consulter les op�rations d'un client");
		System.out.println("4 - Faire un virement ");
		System.out.println("5 - Imprimer un relev� du compte d'un client ");
		System.out.println("6 - Alimenter le compte d'un client");
		System.out.println("7 - Retirer de l'argent d'un compte client");
		System.out.println("8 - Cr�er un compte ");
		System.out.println("9 - Cr�er un client ");
		System.out.println("10 - Changer la domiciliation d'un client");
		System.out.println("11 - Modifier les informations d'un client");
		System.out.println("12 - Cr�er une agence ");
		System.out.println("13 - D�sactiver le compte d'un client");
		System.out.println("14 - D�sactiver un client ");
		System.out.println("15 - Cr�er un conseiller");
		System.out.println();
		System.out.println("Q - Quitter");
		System.out.println("----------------------------------------------");
		
		Scanner in = new Scanner(System.in);
		System.out.println();
		System.out.println("Quelle op�ration souhaitez vous effectuer ?");
		String reponse = in.nextLine();

		switch (reponse) {
		case "1":

		case "2":

		case "3":

		case "4": System.out.println("quel compte souhaitez vous d�biter");
		String compteDebiter = in.nextLine();
		
		System.out.println("quel compte souhaitez vous cr�diter");
		String compteCrediter = in.nextLine();
		
		String montant;
		do {
		System.out.println("quel montant souhaitez vous transf�rer ?");
	 montant = in.nextLine();
		}
		while(!ControleSaisieCompteBancaire.ControleFloat(montant)); 
		
		
		
		CompteBancaire compte1 = new CompteBancaire(compteDebiter);
		CompteBancaire compte2 = new CompteBancaire(compteCrediter);
		
			ServiceBanque.virement(compte1, compte2 , Float.parseFloat(montant), banque);

		case "5":

		case "6":

		case "7":

		case "8": 
			ServiceBanque.creerCompte(banque);
			
			for (Agence agence : banque.getListeAgence()) {
				for (Conseiller conseiller : agence.getListeConseiller()) {
					for (Client client : conseiller.getListeClient()) {
							System.out.println(client);
						}
					}
				}
			
			break;
		
		case "9": 
			ServiceConseiller.creerClient(banque);
			for (Agence agence : banque.getListeAgence()) {
				for (Conseiller conseiller : agence.getListeConseiller()) {
					//for (Client client : conseiller.getListeClient()) {
						System.out.println(conseiller);
					}
				}
			
			break;

		case "10":

		case "11":

		case "12":
			ServiceBanque.creerAgence(banque);
			for (Agence agence : banque.getListeAgence()) {
				System.out.println(agence);
			}
				
			break;

		case "13":

		case "14":
			
		case "15": 
			ServiceAgence.creerConseiller(banque);
			for (Agence agence : banque.getListeAgence()) {
					System.out.println(agence);
				}
			
			break;
			
		case "Q" : System.out.println();
		AffichageChoixProfil.affichageChoix(banque);
		break;

		default:System.out.println();
		System.out.println("Il faut choisir un caract�re VALIDE ");
		System.out.println();
		break;
		}
		
		}while(true);

	}
}


