package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.ControleSaisieCompteBancaire;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.ServiceBanque;
import fr.afpa.services.ServiceClient;

public class AffichageMenuClient {
	
	/**
	 * Affichage menu du profil Client
	 * @param banque
	 */
	
	public static void menuClient(Banque banque) {
		do {
		System.out.println("Menu Client :");
		System.out.println();
		System.out.println("1- Consulter mes informations ");
		System.out.println("2- Consulter mes comptes ");
		System.out.println("3- Consulter mes op�rations ");
		System.out.println("4- Faire un virement ");
		System.out.println("5- Imprimer un relev� de mon compte ");
		System.out.println("6- Alimenter mon compte");
		System.out.println("7- Retirer de l'argent");
		System.out.println();
		System.out.println("Q- Quitter");
		Scanner in = new Scanner(System.in);
		System.out.println();
		System.out.println("Quelle op�ration souhaitez vous effectuer ?");
		String reponse = in.nextLine();

		switch (reponse) {
		case "1":
			ServiceClient.afficherInfoClient(banque);
			break;
		case "2":
			ServiceClient.afficherInfoCompteClient(banque);
			break;
			
		case "3": 
			
		case "4": 
			System.out.println("quel compte souhaitez vous d�biter");
			String compteDebiter = in.nextLine();
			
			System.out.println("quel compte souhaitez vous cr�diter");
			String compteCrediter = in.nextLine();
			
			String montant;
			do {
			System.out.println("quel montant souhaitez vous transf�rer ?");
		 montant = in.nextLine();
			}
			while(!ControleSaisieCompteBancaire.ControleFloat(montant)); 
			
			
			
			CompteBancaire compte1 = new CompteBancaire(compteDebiter);
			CompteBancaire compte2 = new CompteBancaire(compteCrediter);
			
				ServiceBanque.virement(compte1, compte2 , Float.parseFloat(montant), banque);
				break;
			
		case "5":
			
		case "6":
			
		case "7":
		
		case "Q": System.out.println();
		AffichageChoixProfil.affichageChoix(banque);
		default:System.out.println();
		System.out.println("Il faut choisir un caract�re VALIDE ");
		System.out.println();
		break;
		}


	}while(true);
}
}