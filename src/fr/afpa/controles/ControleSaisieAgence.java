package fr.afpa.controles;


import java.util.Scanner;

public class ControleSaisieAgence {
	static String code;
	static String nom;
	static String numeroRue;
	static String  libelleRue;
	static String codePostal;
	static String ville;
	static String adresse;
	
	/*public static String VerifCodeAgence(String codeAgence) {
	Scanner in = new Scanner(System.in); 
	while(!codeAgence.matches("[0-9]{3}")){
		System.out.println("Code Agence INVALIDE, veuillez saisir un nouveau code");
		codeAgence = in.nextLine();
	}
		
		return codeAgence;
	
	}*/
	
	/**
	 * M�thode pour limiter la saisie du nom de l'agence (uniquement des lettres)
	 * @param nomAgence
	 * @return Un nom d'agence correct
	 */
	public static String VerifNomAgence(String nomAgence) {
		
		Scanner in = new Scanner(System.in);
		while (!nomAgence.matches("[a-zA-Z]{1,}")) {
			System.out.println("Nom de l'Agence INVALIDE, veuillez saisir un nouveau nom d'Agence");
						nomAgence = in.nextLine();		
				
		}
		return nomAgence;
		
	}
	
	/**
	 * 	 * M�thode pour limiter la saisie du num�ro de la rue (uniquement des chiffres)
	 * @param numeroRueAgence
	 * @return Un num�ro de rue correct
	 */
	public static String VerifnumeroRue(String numeroRueAgence) {
		Scanner in = new Scanner(System.in);
		while (!numeroRueAgence.matches("[0-9]{1,3}")) {
			System.out.println("Num�ro de rue INVALIDE");
			numeroRueAgence = in.nextLine();
			
		}
		return numeroRueAgence;
		
		}
	
	/**
	 * 	M�thode pour limiter la saisie du libell� de la rue 
	 * @param libelleRueAgence
	 * @return Un libell� de rue correct
	 */
	public static String VeriflibelleRue(String libelleRueAgence ) {
		Scanner in = new Scanner(System.in);
		while (!libelleRueAgence.matches("[a-zA-Z]+( [a-zA-Z]+){1,5}")) {
			System.out.println("Libell� de rue INVALIDE");
			libelleRueAgence = in.nextLine();
		}
		return libelleRueAgence;
		
		}
	
	/**
	 * M�thode pour limiter la saisie du code postal (5 chiffres)

	 * @param codePostalAgence
	 * @return Un code postal correct
	 */
	public static String VerifCodePostalAgence(String codePostalAgence ) {
		Scanner in = new Scanner(System.in);
		while (!codePostalAgence.matches("[0-9]{5}")) {
			
		
			System.out.println("Code postal INVALIDE");
			codePostalAgence = in.nextLine();
		}
		return codePostalAgence;
		
		}
	
	/**
	 * M�thode pour limiter la saisie de la ville (uniquement des lettres)
	 * @param villeAgence
	 * @return Une ville correct
	 */
	public static String VerifVilleAgence(String villeAgence) {
		Scanner in = new Scanner(System.in);
		while(!villeAgence.matches("[a-zA-Z]{2,}")) {
			System.out.println("Nom de Ville invalide");
			villeAgence = in.nextLine();
			
		}
		return villeAgence;
		
	}
	
	/**
	 * M�thode pour regrouper l'adresse avec l'ensemble des donn�es saisies
	 */
	public static void AffichAdresse() {
		System.out.println("L'adresse de l'Agence " + nom  + " ayant pour code " + code +  " est: " + numeroRue + " " + libelleRue + " " + codePostal + " " + ville ) ;
	}
	

	
	
}
