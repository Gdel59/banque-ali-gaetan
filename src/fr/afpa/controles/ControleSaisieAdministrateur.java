package fr.afpa.controles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ControleSaisieAdministrateur {
/**
 * M�thode pour autoriser l'administrateur � se connecter
 * @param in
 * @return true si les donn�es saisies correspondent aux donn�es lues dans le fichier ressources 
 * @throws IOException
 */
	public static boolean validerAuthentificationAdmininistrateur(Scanner in) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("ressources\\administrateur.txt"));
		
		String loginAdmin = br.readLine();
		String mdpAdmin = br.readLine(); 
		br.close();
		do {	
		System.out.println("Entrer votre identifiant :");
		String saisieLoginAdmin = in.nextLine();
		System.out.println("Entrer votre mot de Passe :");
		String saisieMdpAdmin = in.nextLine();
		
		if (saisieLoginAdmin.equals(loginAdmin) && saisieMdpAdmin.equals(mdpAdmin)) 
		{
			System.out.println("Bienvenue");
			return true;
		}else {
			System.out.println("Authentification impossible !");
		}
		}while(true);
		
		
		
	}
	
}
