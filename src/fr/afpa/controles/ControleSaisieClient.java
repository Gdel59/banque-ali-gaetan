package fr.afpa.controles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class ControleSaisieClient {

	/**
	 * M�thode pour limiter la saisie du nom du client (uniquement des lettres)
	 * @param nomClient
	 * @return Un nom correct
	 */
	public static boolean validerNom(String nomEntree) {
		Scanner in = new Scanner(System.in);

		while (true) {
			if (nomEntree != null && nomEntree.matches("[a-zA-Z]{2,}")) {
				return true;
			} else {
				System.out.println("Veuillez saisir un nom valide");
				nomEntree = in.nextLine();

			}
		}
	}
	/**
	 * M�thode pour limiter la saisie du pr�nom du client (uniquement des lettres)
	 * @param prenomClient
	 * @return Un pr�nom correct
	 */
	public static boolean validerPrenom(String prenomEntree) {
		Scanner in = new Scanner(System.in);
		while (true) {
			if (prenomEntree != null && prenomEntree.matches("[a-zA-Z]{2,}")) {
				return true;
			} else {
				System.out.println("Veuillez saisir un pr�nom valide");
				
				prenomEntree = in.nextLine();

			}

		}
	}
	/**
	 * M�thode pour valider la date saisie (format valide : JJ/MM/AAAA)
	 * @param dateAVerifier
	 * @return Une datte correct
	 */
	public static boolean verifierDateValide(String dateAVerifier) {
		Scanner in = new Scanner(System.in);
while(true) {
		try {
			String[] conversionDate = dateAVerifier.split("/");
			LocalDate.parse(conversionDate[2] + "-" + conversionDate[1] + "-" + conversionDate[0]);
			return true;
		} catch (DateTimeParseException | ArrayIndexOutOfBoundsException gr) {
			System.out.println("Date Invalide, veuiller saisir une date au format JJ/MM/AAAA");
			dateAVerifier = in.nextLine();
		}
		}
	}
	/**
	 * M�thode pour valider l'email saisie (format valide : a.a@aaaa.fr)
	 * @param mail A Verifier
	 * @return Un mail correct
	 */
	public static boolean validerEmail(String emailEntree) {
		Scanner in = new Scanner(System.in);

		while (true) {
			if (emailEntree != null
					&& emailEntree.matches("[A-Za-z]{1,}\\.[a-zA-Z0-9]{1,12}@[a-zA-Z0-9]{4,8}\\.[a-zA-Z]{2,3}")) {
				return true;
			} else {
				System.out.println("Veuillez saisir une adresse mail valide");
				emailEntree = in.nextLine();
			}

		}

	}
	/**
	 * M�thode pour valider l'identifiant saisie (format valide : AA123456)
	 * @param identifiant A Verifier
	 * @return Un identifiant correct
	 */
	public static boolean validerIdentifiant(String identifiantEntree) {
		Scanner in = new Scanner(System.in);

		while (true) {
			if (identifiantEntree != null && identifiantEntree.matches("[A-Z]{2}[0-9]{6}")) {
				return true;
			} else {
				System.out.println("Veuillez saisir un identifiant valide");
				identifiantEntree = in.nextLine();
			}

		}
	}
	
	/**
	 * M�thode pour autoriser le client � se connecter
	 * @param in
	 * @return true si les donn�es saisies correspondent aux donn�es lues dans le fichier ressources 
	 */
	public static boolean verifLoginClient(String loginClient) {
		try
		{
			FileReader fr = new FileReader("Ressources\\LoginMdpClients.txt");
			BufferedReader br = new BufferedReader(fr);
			String loginClient1;
			while(br.ready()) {
			 loginClient1 = br.readLine();
			if (loginClient.equals(loginClient1.split("~")[0])) {
				br.close();
				return true;
			}
			}
			br.close();
			System.out.println("Login Incorrect !");
			return false;
		}
	catch (Exception e)
		{
		System.out.println("Erreur"+e);
		}
		return false;
	}
	
	/**
	 * M�thode pour autoriser le client � se connecter
	 * @param in
	 * @return true si les donn�es saisies correspondent aux donn�es lues dans le fichier ressources 
	 */
	public static boolean verifMdpClient(String mdpClient) {
		try
		{
			FileReader fr = new FileReader("Ressources\\LoginMdpClients.txt");
			BufferedReader br = new BufferedReader(fr);
			String mdpClient1;
			while(br.ready()) {
			 mdpClient1 = br.readLine();
		
			if (mdpClient.equals(mdpClient1.split("~")[1])) {
				br.close();
				return true;
			}
			}
			br.close();
				System.out.println("Mot de passe incorrect");
				return false;
		}
	catch (Exception e)
		{
		System.out.println("Erreur"+e);
		}
		return false;
	}
}
