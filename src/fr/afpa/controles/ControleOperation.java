package fr.afpa.controles;

import fr.afpa.entite.CompteBancaire;

public class ControleOperation {
	
	/**
	 * Methode pour controler que le compte ne puisse pas effectuer de virement s'il est en d�couvert
	 * @param compte A d�biter
	 * @param montant A transf�rer
	 * @return false si le compte est � d�couvert
	 */
	public static boolean controleSoldeValide(CompteBancaire compte, Float montant) {
		if(compte.getSoldeCompte()-montant < 0 && compte.getDecouvertAutorise()==false) {
			System.out.println("Op�ration impossible, d�couvert non autoris�");
				return false;
}
		return true;
	}
}
