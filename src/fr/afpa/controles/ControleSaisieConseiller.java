package fr.afpa.controles;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class ControleSaisieConseiller {
	/**
	 * M�thode pour valider le login saisie (format valide : CO1234)
	 * @param login A Verifier
	 * @return Un login correct
	 */
	public static boolean verifCreationLoginConseiller(String loginConseiller) {
		
		if(loginConseiller.length()==6) {   
			
			if(loginConseiller.substring(0,2).equals("CO")) {
			
				String a= loginConseiller.substring(3,loginConseiller.length());
				
				try{ 
					Integer.parseInt(a);
					return true;
				}
				catch(NumberFormatException e) 
				{
					return false;
				}
					
				}
			return false;
			}
			else 
			return false;
	}
		
	/**
	 * M�thode pour autoriser le conseiller � se connecter
	 * @param in
	 * @return true si les donn�es saisies correspondent aux donn�es lues dans le fichier ressources 
	 */
	public static boolean verifLoginConseiller(String loginConseiller) {
		try
		{
			FileReader fr = new FileReader("Ressources\\LoginMdpConseiller.txt");
			BufferedReader br = new BufferedReader(fr);
			String loginConseiller1;
			while(br.ready()) {
			 loginConseiller1 = br.readLine();
			if (loginConseiller.equals(loginConseiller1.split("~")[0])) {
				br.close();
				return true;
			}
			}
			br.close();
			System.out.println("Login Incorrect !");
			return false;
		}
	catch (Exception e)
		{
		System.out.println("Erreur"+e);
		}
		return false;
	}
	
	/**
	 * M�thode pour autoriser le conseiller � se connecter
	 * @param in
	 * @return true si les donn�es saisies correspondent aux donn�es lues dans le fichier ressources 
	 */
	public static boolean verifMdpConseiller(String mdpConseiller) {
		try
		{
			FileReader fr = new FileReader("Ressources\\LoginMdpConseiller.txt");
			BufferedReader br = new BufferedReader(fr);
			String mdpConseiller1;
			while(br.ready()) {
			 mdpConseiller1 = br.readLine();
		
			if (mdpConseiller.equals(mdpConseiller1.split("~")[1])) {
				br.close();
				return true;
			}
			}
			br.close();
				System.out.println("Mot de passe incorrect");
				return false;
		}
	catch (Exception e)
		{
		System.out.println("Erreur"+e);
		}
		return false;
	}
	

}
